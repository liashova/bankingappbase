﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    class Account
    {
        private int _acctNo;

        private string _acctHolderName;

        protected double _balance;

        protected float _annualIntrRate;

        public Account(int acctNo, string acctHolderName)
        {
            _acctNo = acctNo;
            _acctHolderName = acctHolderName;
            _balance = 0;
            _annualIntrRate = 0.0f;
        }

        public int AccountNumber
        {
            get => _acctNo;
            set => _acctNo = value;
        }

        public string AcctHolderName
        {
            get => _acctHolderName;
            set => _acctHolderName = value;
        }

        public virtual float AnnualIntrRate
        {
            get => _annualIntrRate;
            set => _annualIntrRate = value;
        }
        
        public float MonthlyIntrRate
        {
            get => _annualIntrRate / 12;
        }

        public double Balance
        {
            get => _balance;
        }

        public double Deposit(double ammount)
        {   
            //TODO: implement deposit
            return 0;
        }

        public double Withdraw(double amount)
        {
            return 0;
        }

        public void Load(StreamReader acctFileReader)
        {
        }

        public void Save(StreamWriter acctFileWriter)
        {
        }

        public override string ToString()
        {
            return $"Account for {_acctHolderName} with balance {_balance}";
        }
    }
}
