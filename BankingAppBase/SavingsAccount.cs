﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    class SavingsAccount : Account
    {
        public SavingsAccount(int acctNo, string acctHolderName) : base(acctNo, acctHolderName)
        {
        }
    }
}
